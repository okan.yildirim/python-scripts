from json import loads
from kafka import KafkaConsumer

consumer = KafkaConsumer(
    'my-consumer-topic-name',
    bootstrap_servers=['localhost:9092'],
    auto_offset_reset='earliest',
    enable_auto_commit=True,
    group_id='my-group-id',
    value_deserializer=lambda x: loads(x.decode('utf-8')))

for message in consumer:
    message = message.value
    print('{} consumed'.format(message))
