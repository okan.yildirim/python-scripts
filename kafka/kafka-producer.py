from json import dumps
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers=['10.250.223.130:9092'],
                         value_serializer=lambda x:
                         dumps(x).encode('utf-8'))
message = {
    'name': 'okan',
    'age': 26
}
producer.send('my-producer-topic', value=message)
