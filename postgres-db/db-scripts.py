import pg8000

try:
    conn = pg8000.connect(user='myuser', host='localhost', port=5432,
                          database='users', password='my-password')
    db = conn
    cursor = db.cursor()
except pg8000.Error:
    print("Database connect failed")


cursor.execute(
        "SELECT id, name, is_active FROM users "
        "WHERE age > 24 "
        "OR is_active is not null;"
)

results = cursor.fetchall()

for result in results:
    userId = result[0]
    name = result[1]
    isActive = result[2]
    print("userId: " + userId + " name: " + name + " is_active: " + isActive)


for userId in range(0, 100):
    cursor.execute('UPDATE users SET is_active = false where is_active is null and seller_id = ' + str(userId))
    conn.commit()
    print (str(userId) + ' done')
