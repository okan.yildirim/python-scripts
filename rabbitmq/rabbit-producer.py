import pika
import json

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost',
                              port=5672,
                              credentials=pika.credentials.PlainCredentials("admin", "admin")))

channel = connection.channel()

msg = {
    "id": 4074,
    "name": "okan",
    "birthDate": 1604696399000
}

dumps = json.dumps(msg)
print(dumps)

channel.basic_publish(exchange='my.exchange',
                      routing_key='my.routing.key',
                      body=dumps)
print("""{id} published""".format(id=4074))

connection.close()
