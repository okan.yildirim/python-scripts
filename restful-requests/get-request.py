from pip._vendor import requests

userId = 4074
name = 'okan'

response = requests.get("""https://myrestfulapi/users?id={id}&name={name}"""
                        .format(id=userId, name=name))

if response.status_code == 200:
    print("Get request is successful")
    json = response.json()
    print("id: " + json[0]["id"] + " name: " + json[0]["name"])
else:
    print("Get request is unsuccessful")
